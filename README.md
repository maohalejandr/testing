## Description

Aplicación la cual cuenta las quías generadas a través del portal Envia.com con sockets y la muestra en un index.html, la aplicación recupera la información de una API cada cierto tiempo para luego enviar y mostrar la información en una página html teniendo un canal de comunicación abierto gracias a los sockets; Esta aplicación fue creada utilizando node como lenguaje de programación y nest como framework

NOTA: una vez que el proyecto este levantado se apreciara el contador abriendo el archivo index.html de la carpeta static.
Si la instalación falla intente borrar la carpeta node_modules y volver a instalar el proyecto.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```
