import {  WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, WebSocketServer} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { Cron } from '@nestjs/schedule';
let numberGuias = 0;

@WebSocketGateway()
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

 @WebSocketServer() server: Server;
 private logger: Logger = new Logger('AppGateway');

 @Cron('*/2 * * * * *')
   async taskSerive() {
    var axios = require('axios');
    this.logger.log('Revisando nuevas guias..');

    const config = {
      method: 'post',
      url: 'http://localhost:3000',
      headers: {},
      data : {}
    };

    const response = await axios(config)
    numberGuias = response.data.data.length;
    this.server.emit('msgToClient', numberGuias);
  }  

 afterInit(server: Server) {
  this.logger.log('Init');
 }

 handleDisconnect(client: Socket) {
  this.logger.log(`Client disconnected: ${client.id}`);
 }

 handleConnection(client: Socket, ...args: any[]) {
  this.logger.log(`Client connected: ${client.id}`);
 }
}