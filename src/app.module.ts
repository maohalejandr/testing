import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { AppGateway } from './app.gateway';



@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),    
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService, AppGateway],
})
export class AppModule {}
