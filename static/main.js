const app = new Vue({
    el: '#app',
    data: {
     title: 'Nestjs Websockets Chat',
     messages: 'Esperando respuesta..',
     socket: null
    },
    methods: {
    receivedMessage(message) {
     this.messages= message;
    },
    validateInput() {
     return this.name.length > 0 && this.text.length > 0
    }
   },
    created() {
     this.socket = io('http://localhost:3000')
     this.socket.on('msgToClient', (message) => {
      this.receivedMessage(message)
     })
    }
   })