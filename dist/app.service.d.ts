export declare class AppService {
    getHello(): string;
    getDataWebhook(): Promise<any>;
}
